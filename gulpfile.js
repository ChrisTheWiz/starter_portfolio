const {src, dest, watch, series, parallel} = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const babel = require('gulp-babel');


const files = {
    scssPath: 'app/scss/**/*.scss',
    jsPath: 'app/js/**/*.js',
    jsDistPath: 'dist/js',
    cssDistPath: 'dist/css'
};

function scssTask() {
    return src(files.scssPath)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(sourcemaps.write('.'))
        .pipe(dest(files.cssDistPath)
        );
}

function jsTask() {
    return src([
        files.jsPath
    ])
        .pipe(concat('bundle.min.js'))
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify())
        .pipe(dest(files.jsDistPath)
        );
}

function watchTask() {
    watch([files.scssPath, files.jsPath, 'gulpfile.js', 'index.html'],
        parallel(scssTask, jsTask)
    );
}

exports.default = series(
    parallel(scssTask, jsTask),
    watchTask
);
