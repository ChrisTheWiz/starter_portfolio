let mainNavLinks = document.querySelectorAll("#navigation-bar ul li button");
// let mainSections = document.querySelectorAll("main section[id]:before");

let viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
let viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
let rem = parseFloat(getComputedStyle(document.documentElement).fontSize);

function isMicrosoft() {
    return navigator.userAgent.indexOf('Trident') > -1 || navigator.userAgent.indexOf('Edge') > -1;
}

function isIE() {
    return navigator.userAgent.indexOf('Trident') > -1;
}


// The scroll from JavaScript does not work in Internet explorer, so we might as well
// remove the navigation bar
if (isIE()) {
    let element = document.getElementById('navigation-bar');
    element.parentNode.removeChild(element);
}


for (let i = 0; i < mainNavLinks.length; i++) {
    let link = mainNavLinks[i];
    link.addEventListener('click', () => {
        let targetSection = document.getElementById(link.getAttribute('target'));
        if (isMicrosoft()) {
            window.scrollTo(0, targetSection.offsetTop - 10 * rem);
        } else {
            window.scrollTo({
                top: targetSection.offsetTop - 10 * rem,
                behavior: 'smooth'
            });
        }
    })
}

let throttledScrollSpy = throttle(() => {
    let fromTop = window.scrollY;

    for (let i = 0; i < mainNavLinks.length; i++) {
        let link = mainNavLinks[i];
        let section = document.getElementById(link.getAttribute('target'));
        if (
            section.offsetTop <= fromTop + 10 * rem &&
            section.offsetTop + section.offsetHeight > fromTop + 10 * rem
        ) {
            link.classList.add("current");
        } else {
            link.classList.remove("current");
        }
    }
}, 100);


if (!isIE()) {
    throttledScrollSpy();
    window.addEventListener("scroll", () => {
        throttledScrollSpy();
    });
}

let elements = document.querySelectorAll("h1,h2,.highlight,p,.contact-box,.card-title,.card-content,.card-action-visit,span,.back-to-top, .card-details li");
let elemesWithFadeableBackgrounds = document.querySelectorAll(".card-details");

let opacityChangerThrottleFactor;
if (isIE()) {
    opacityChangerThrottleFactor = 200;
} else {
    opacityChangerThrottleFactor = 50;
}

let throttledOpacityChanger = throttle(() => {
    for (let i = 0; i < elements.length; i++) {
        let element = elements[i];
        let relPos = element.getBoundingClientRect().top;
        if (viewportWidth <= 675 || viewportHeight <= 760) {
            if (relPos >= -20 || relPos <= 110) {
                // formula is (relPos - distance_where_elem_invisible) / length_of_disappearing_act
                let opacity = (relPos - 40) / 60;
                element.style.opacity = opacity.toString();
            }
        } else {
            if (relPos >= -20 || relPos <= 200) {
                let opacity = (relPos - (rem * 3)) / 112;
                element.style.opacity = opacity.toString();
            }
        }
    }

    for (let i = 0; i < elemesWithFadeableBackgrounds.length; i++) {
        let elem = elemesWithFadeableBackgrounds[i];
        let relPos = elem.getBoundingClientRect().top;
        if (relPos >= -20 || relPos <= 200) {
            let opacity = (relPos - (rem * 3)) / 112;
            elem.style.backgroundColor = `rgba(51,51,51,${opacity}`;
        }

    }
}, opacityChangerThrottleFactor);

window.addEventListener("scroll", throttledOpacityChanger);
