# Starter Portfolio for Aspiring Web Devs!

This is a simple, yet elegant, responsive cross-browser portfolio template.

It uses JavaScript for the text fade and ScrollSpy-like effect (*no jQuery!*).

#### It uses:
* SASS, with scss files
* Gulp, for building SASS (compilation, auto-prefixes and minification) and bundling JS (scripts concatenation, babelification, minimization)
* Bootstrap Reboot for default styling, included in /app/scss/_reboot.scss
* rfs (which stands for Responsive Font Sizes)
* Lodash.throttle for callbacks on `scroll` event
* Google Fonts: Raleway and Montserrat (regular and 700), included in the /assets folder

#### To make it yours:
* Clone the project
* Run `npm install`
* Run `gulp`
* Start developing!

##### When you're done, just serve `index.html`, `assets/` and `dist/`
#### Also:
 You can add a favicon. Just put it in the /assets directory and uncomment the 
 ```
<!--    <link rel="icon" type="image/x-icon" href="assets/favicon.ico">-->
```
from index.html

The website is fully responsive, and from a certain viewport size switches to touch based interaction.

The smooth scrolling fails gracefully on Edge, while on Internet Explorer the navigation bar disappears completely.
